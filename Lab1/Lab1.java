import java.io.IOException;
import java.util.*;

public class Lab1 {
    public static void main(String[] args) throws IOException {
        FileParser a = new FileParser();
        a.readFile("input.txt");
        a.mapToSortList();
        a.writeToOutputFile("output.csv");
    }
}

import java.io.*;
import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.Collectors;


public class FileParser {
    static final String csv_separating_mark = ";";
    static final String value_format = "%";
    static final String file_separating_mark = " ";
    int word_count;
    Map<String, Integer> wordMap;
    List<Map.Entry<String, Integer>> list;

    void writeToOutputFile(String outputFileName) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputFileName))) {
            for (Map.Entry<String, Integer> i : list) {
                writer.write(i.getKey() + csv_separating_mark + String.format("%.2f", ((double)i.getValue()/word_count*100)) + value_format);
                writer.newLine();
            }
        }
    }

    void readFile(String inputFileName) throws IOException {
        try (BufferedReader inputFile = new BufferedReader(new FileReader(inputFileName))) {
            String csvLine = null;
            wordMap = new HashMap<>();
            while ((csvLine = inputFile.readLine()) != null) {
                for (String i : csvLine.split(file_separating_mark)) {
                    if (wordMap.merge(i, 1, (a, b) -> (a + b)) == 1) {
                        word_count++;
                    }
                }
            }
        }
    }

    void mapToSortList() {
        list = new ArrayList<>(wordMap.entrySet());
        list = list.stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).collect(Collectors.toList());
    }

}

